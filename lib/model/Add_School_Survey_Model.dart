class AddSchoolUiModel {
  bool? success;
  String? message;
  List<SurveyQuestions>? surveyQuestions;

  AddSchoolUiModel({this.success, this.message, this.surveyQuestions});

  AddSchoolUiModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    if (json['Survey_Questions'] != null) {
      surveyQuestions = <SurveyQuestions>[];
      json['Survey_Questions'].forEach((v) {
        surveyQuestions!.add(SurveyQuestions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['success'] = success;
    data['message'] = message;
    if (surveyQuestions != null) {
      data['Survey_Questions'] =
          surveyQuestions!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SurveyQuestions {
  int? questionId;
  int? questionBankId;
  ControlTypeId? controlTypeId;
  List<ValidationLookup>? validationLookup;
  String? question;
  List<Option>? option;
  List<Null>? subquestion;

  SurveyQuestions(
      {this.questionId,
      this.questionBankId,
      this.controlTypeId,
      this.validationLookup,
      this.question,
      this.option,
      this.subquestion});

  SurveyQuestions.fromJson(Map<String, dynamic> json) {
    questionId = json['QuestionId'];
    questionBankId = json['QuestionBankId'];
    controlTypeId = json['ControlTypeId'] != null
        ? ControlTypeId.fromJson(json['ControlTypeId'])
        : null;
    if (json['ValidationLookup'] != null) {
      validationLookup = <ValidationLookup>[];
      json['ValidationLookup'].forEach((v) {
        validationLookup!.add(ValidationLookup.fromJson(v));
      });
    }
    question = json['Question'];
    if (json['Option'] != null) {
      option = <Option>[];
      json['Option'].forEach((v) {
        option!.add(Option.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['QuestionId'] = questionId;
    data['QuestionBankId'] = questionBankId;
    if (controlTypeId != null) {
      data['ControlTypeId'] = controlTypeId!.toJson();
    }
    if (validationLookup != null) {
      data['ValidationLookup'] =
          validationLookup!.map((v) => v.toJson()).toList();
    }
    data['Question'] = question;
    if (option != null) {
      data['Option'] = option!.map((v) => v.toJson()).toList();
    }

    return data;
  }
}

class ControlTypeId {
  int? controlTypeId;
  String? controlType;
  int? isMultipleOptions;

  ControlTypeId({this.controlTypeId, this.controlType, this.isMultipleOptions});

  ControlTypeId.fromJson(Map<String, dynamic> json) {
    controlTypeId = json['ControlTypeId'];
    controlType = json['ControlType'];
    isMultipleOptions = json['IsMultipleOptions'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ControlTypeId'] = controlTypeId;
    data['ControlType'] = controlType;
    data['IsMultipleOptions'] = isMultipleOptions;
    return data;
  }
}

class ValidationLookup {
  int? questionValidationId;
  int? questionId;
  int? validationId;
  String? validation;

  ValidationLookup(
      {this.questionValidationId,
      this.questionId,
      this.validationId,
      this.validation});

  ValidationLookup.fromJson(Map<String, dynamic> json) {
    questionValidationId = json['QuestionValidationId'];
    questionId = json['QuestionId'];
    validationId = json['ValidationId'];
    validation = json['Validation'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['QuestionValidationId'] = questionValidationId;
    data['QuestionId'] = questionId;
    data['ValidationId'] = validationId;
    data['Validation'] = validation;
    return data;
  }
}

class Option {
  int? optionId;
  String? option;

  Option({this.optionId, this.option});

  Option.fromJson(Map<String, dynamic> json) {
    optionId = json['OptionId'];
    option = json['Option'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['OptionId'] = optionId;
    data['Option'] = option;
    return data;
  }
}
