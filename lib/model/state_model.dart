class StateModel {
  bool? success;
  String? message;
  List<States>? state;

  StateModel({this.success, this.message, this.state});

  StateModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    if (json['state'] != null) {
      state = <States>[];
      json['state'].forEach((v) {
        state!.add(States.fromJson(v));
      });
    }
  }
}

class States {
  int? stateId;
  int? countryId;
  String? name;
  int? isActive;

  States({this.stateId, this.countryId, this.name, this.isActive});

  States.fromJson(Map<String, dynamic> json) {
    stateId = json['StateId'];
    countryId = json['CountryId'];
    name = json['Name'];
    isActive = json['IsActive'];
  }
}
