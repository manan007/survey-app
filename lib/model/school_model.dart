class GetSchoolModel {
  bool? success;
  String? message;
  List<SchoolList>? schoolList;

  GetSchoolModel({this.success, this.message, this.schoolList});

  GetSchoolModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    if (json['school_list'] != null) {
      schoolList = <SchoolList>[];
      json['school_list'].forEach((v) {
        schoolList!.add(SchoolList.fromJson(v));
      });
    }
  }


}

class SchoolList {
  int? schoolId;
  String? name;
  String? address;
  String? latLong;
  String? phoneNumber;
  String? email;
  String? website;
  int? stateid;
  String? state;
  String? city;
  int? pinCode;
  List? questionBank;
  String? mediums;
  String? mediumids;

  SchoolList(
      {this.schoolId,
        this.name,
        this.address,
        this.latLong,
        this.phoneNumber,
        this.email,
        this.website,
        this.stateid,
        this.state,
        this.city,
        this.pinCode,
        this.questionBank,
        this.mediums,
        this.mediumids});

  SchoolList.fromJson(Map<String, dynamic> json) {
    schoolId = json['SchoolId'];
    name = json['Name'];
    address = json['Address'];
    latLong = json['LatLong'];
    phoneNumber = json['PhoneNumber'];
    email = json['Email'];
    website = json['Website'];
    stateid = json['Stateid'];
    state = json['State'];
    city = json['City'];
    pinCode = json['PinCode'];
    if (json['QuestionBank'] != null) {
      questionBank = <Null>[];

    }
    mediums = json['mediums'];
    mediumids = json['Mediumids'];
  }


}