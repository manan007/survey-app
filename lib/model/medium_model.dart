class MediumModel {
  bool? success;
  String? message;
  List<SchoolList>? schoolList;
  List<String> medium = [];

  MediumModel({this.success, this.message, this.schoolList});

  MediumModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    if (json['school_list'] != null) {
      schoolList = <SchoolList>[];
      json['school_list'].forEach((v) {
        medium.add(v['Instruction']);
        schoolList!.add(SchoolList.fromJson(v));
      });
    }
  }
}

class SchoolList {
  int? instructionId;
  String? instruction;
  int? isActive;

  SchoolList({this.instructionId, this.instruction, this.isActive});

  SchoolList.fromJson(Map<String, dynamic> json) {
    instructionId = json['InstructionId'];
    instruction = json['Instruction'];
    isActive = json['IsActive'];
  }
}
