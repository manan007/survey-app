class CountryModel {
  bool? success;
  String? message;
  List<Country>? country;

  CountryModel({this.success, this.message, this.country});

  CountryModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    if (json['country'] != null) {
      country = <Country>[];
      json['country'].forEach((v) {
        country!.add(Country.fromJson(v));
      });
    }
  }
}

class Country {
  int? countryId;
  String? name;
  int? isActive;

  Country({this.countryId, this.name, this.isActive});

  Country.fromJson(Map<String, dynamic> json) {
    countryId = json['CountryId'];
    name = json['Name'];
    isActive = json['IsActive'];
  }
}
