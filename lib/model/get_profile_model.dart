class GetProfileModel {
  bool? success;
  String? message;
  List<Userlist>? userlist;

  GetProfileModel({this.success, this.message, this.userlist});

  GetProfileModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    if (json['userlist'] != null) {
      userlist = <Userlist>[];
      json['userlist'].forEach((v) {
        userlist!.add(new Userlist.fromJson(v));
      });
    }
  }


}

class Userlist {
  int? userId;
  int? roleId;
  String? country;
  String? state;
  String? fullName;
  String? email;
  String? mobileNo;
  String? photo;
  String? aboutMe;
  int? stateId;
  int? createdBy;

  Userlist(
      {this.userId,
        this.roleId,
        this.country,
        this.state,
        this.fullName,
        this.email,
        this.mobileNo,
        this.photo,
        this.aboutMe,
        this.stateId,
        this.createdBy});

  Userlist.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    roleId = json['RoleId'];
    country = json['Country'];
    state = json['State'];
    fullName = json['Full_Name'];
    email = json['Email'];
    mobileNo = json['MobileNo'];
    photo = json['Photo'];
    aboutMe = json['AboutMe'];
    stateId = json['StateId'];
    createdBy = json['CreatedBy'];
  }


}