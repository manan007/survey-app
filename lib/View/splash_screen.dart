import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:survey/helper/global_variable.dart';

class splash_screen extends StatefulWidget {
  @override
  splash_screenState createState() => splash_screenState();
}

class splash_screenState extends State<splash_screen> {
  bool isLogin = false;
  bool isCheckIng = false;

  startTime() async {
    var _duration = const Duration(seconds: 2);
    return Timer(_duration, getSharedPreferences);
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
                height: MediaQuery.of(context).size.height * 0.85,
                child: Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    "assets/images/Page 2__mobile + robo.png",
                    width: MediaQuery.of(context).size.width * 0.6,
                    height: 200,
                  ),
                )),
            Text(
              'Scrappy Survey',
              style: TextStyle(
                  color: Colors.blue[800], fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    ));
  }

  Future<String?> getSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isLogin = prefs.getBool("isLogin") ?? false;
    userId.value = prefs.getInt("userid") ?? 0;
    setState(() {
      isCheckIng = false;
      isCheckIng == true
          ? Container()
          : isLogin == true
              ? Navigator.pushNamed(context, '/DashBord')
              : Navigator.pushNamed(context, '/HomePage');
    });
  }
}
