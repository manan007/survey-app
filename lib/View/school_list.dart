import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:survey/View/edit_school.dart';
import 'package:survey/View/school_deatils_show.dart';
import 'package:survey/helper/const.dart';
import 'package:survey/helper/global_variable.dart';

class SchoolList extends StatefulWidget {
  const SchoolList({Key? key}) : super(key: key);

  @override
  _SchoolListState createState() => _SchoolListState();
}

class _SchoolListState extends State<SchoolList> {
  //Confirmation dialoug

  Future<void> _showMyDialog(int? schoolID) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Alert !"),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('Are you sure you want to delete this school?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                primary: Colors.red,
              ),
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Yes'),
              onPressed: () async {
                bool result = await schoolRepo.deleteSchool(schoolID);
                Navigator.of(context).pop();
                if (result) {
                  getData();
                }
              },
            ),
          ],
        );
      },
    );
  }

  bool apiCalling = false;

  void getData() async {
    setState(() {
      apiCalling = true;
    });

    await schoolRepo.getSchoolList();

    setState(() {
      apiCalling = false;
    });
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        leading: Builder(
            builder: (context) => IconButton(
                  onPressed: () {
                    setState(() {
                      Navigator.pop(context);
                    });
                  },
                  icon: Icon(Icons.arrow_back),
                  color: Colors.black,
                )),
        backgroundColor: Colors.white,
        title: Container(
            height: 70,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/Band 04 copy.png'))),
            child: Center(child: Text("School List"))),
      ),
      body: Container(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 15,
              ),
              apiCalling == true
                  ? SizedBox(
                      height: h * 0.8,
                      width: w,
                      child: const Center(child: CircularProgressIndicator()))
                  : ValueListenableBuilder(
                      valueListenable: schoolRepo.noSchoolFound,
                      builder: (context, a, v) {
                        return schoolRepo.noSchoolFound.value == true
                            ? Column(
                                children: [
                                  SizedBox(
                                    height: h * 0.25,
                                  ),
                                  Image.asset('assets/images/add_school.jpg'),
                                  SizedBox(
                                    height: h * 0.05,
                                  ),
                                  const Center(
                                    child: Text(
                                      "No School Added Yet!",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          fontSize: 19,
                                          color: Colors.blue),
                                    ),
                                  ),
                                ],
                              )
                            : ListView.builder(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: schoolRepo
                                    .getSchoolModel.schoolList!.length,
                                itemBuilder: (context, v) {
                                  return Column(
                                    children: [
                                      Container(
                                        height: h * 0.3,
                                        width: w,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(7),
                                          boxShadow: const [
                                            BoxShadow(
                                                color: Colors.grey,
                                                offset: Offset(0, 2),
                                                blurRadius: 20,
                                                spreadRadius: 0)
                                          ],
                                        ),
                                        child: Container(
                                          margin: const EdgeInsets.all(10),
                                          child: Row(
                                            children: [
                                              const SizedBox(
                                                width: 20.0,
                                              ),
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  SizedBox(
                                                    width: w * 0.7,
                                                    child: Text(
                                                      "${schoolRepo.getSchoolModel.schoolList![v].name}",
                                                      textScaleFactor: 0.85,
                                                      style: const TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 25,
                                                          fontWeight:
                                                              FontWeight.w800),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    height: 15.0,
                                                  ),
                                                  SizedBox(
                                                    width: w * 0.8,
                                                    child: Text(
                                                      "${schoolRepo.getSchoolModel.schoolList![v].address}",
                                                      textScaleFactor: 0.85,
                                                      style: const TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 17,
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    height: 10.0,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      // schoolRepo
                                                      //             .getSchoolModel
                                                      //             .schoolList![
                                                      //                 v]
                                                      //             .questionBankId ==
                                                      //         0
                                                      //     ?

                                                      GestureDetector(
                                                        onTap: () {
                                                          Navigator.pushNamed(
                                                              context,
                                                              '/SchoolSurvey',
                                                              arguments: schoolRepo
                                                                  .getSchoolModel
                                                                  .schoolList![
                                                                      v]
                                                                  .schoolId);
                                                        },
                                                        child: Container(
                                                          height: 40,
                                                          width: w * 0.4,
                                                          decoration: BoxDecoration(
                                                              color:
                                                                  backGroundColor,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5)),
                                                          child: const Center(
                                                            child: Text(
                                                              "Add Survey",
                                                              textScaleFactor:
                                                                  0.85,
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700),
                                                            ),
                                                          ),
                                                        ),
                                                      ),

                                                      const SizedBox(
                                                        width: 15.0,
                                                      ),
                                                      Container(
                                                        height: 40,
                                                        width: w * 0.4,
                                                        decoration: BoxDecoration(
                                                            color: const Color(
                                                                0xff5CDD78),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5)),
                                                        child: const Center(
                                                          child: Text(
                                                            "Edit Survey",
                                                            textScaleFactor:
                                                                0.85,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 16,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const SizedBox(
                                                    height: 15.0,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      GestureDetector(
                                                        onTap: () async {
                                                          if (kDebugMode) {
                                                            print("MediumData" +
                                                                schoolRepo
                                                                    .getSchoolModel
                                                                    .schoolList![
                                                                        v]
                                                                    .mediums
                                                                    .toString());
                                                          }
                                                          bool result =
                                                              await Navigator
                                                                  .push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  (context) =>
                                                                      EditSchool(
                                                                schoolId: schoolRepo
                                                                    .getSchoolModel
                                                                    .schoolList![
                                                                        v]
                                                                    .schoolId,
                                                                webSite:
                                                                    '${schoolRepo.getSchoolModel.schoolList![v].website}',
                                                                city:
                                                                    '${schoolRepo.getSchoolModel.schoolList![v].city}',
                                                                pinCode:
                                                                    '${schoolRepo.getSchoolModel.schoolList![v].pinCode}',
                                                                address:
                                                                    '${schoolRepo.getSchoolModel.schoolList![v].address}',
                                                                email:
                                                                    '${schoolRepo.getSchoolModel.schoolList![v].email}',
                                                                phoneNumber:
                                                                    '${schoolRepo.getSchoolModel.schoolList![v].phoneNumber}',
                                                                schoolName:
                                                                    '${schoolRepo.getSchoolModel.schoolList![v].name}',
                                                                state:
                                                                    '${schoolRepo.getSchoolModel.schoolList![v].state}',
                                                                medium:
                                                                    '${schoolRepo.getSchoolModel.schoolList![v].mediums}',
                                                                stateId: schoolRepo
                                                                    .getSchoolModel
                                                                    .schoolList![
                                                                        v]
                                                                    .stateid,
                                                                Mediumids:
                                                                    '${schoolRepo.getSchoolModel.schoolList![v].mediums}',
                                                              ),
                                                            ),
                                                          );
                                                          if (result) {
                                                            getData();
                                                          }
                                                        },
                                                        child: Container(
                                                          height: 30,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width /
                                                              3,
                                                          decoration: BoxDecoration(
                                                              color: const Color(
                                                                  0xffF4F3FC),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5)),
                                                          child: const Center(
                                                            child: Text(
                                                              "Edit ",
                                                              textScaleFactor:
                                                                  0.85,
                                                              style: TextStyle(
                                                                  color: Color(
                                                                      0xff634DD1),
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        width: 50.0,
                                                      ),
                                                      GestureDetector(
                                                        onTap: () async {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder:
                                                                      (context) =>
                                                                          SchoolDetailsShow(
                                                                            webSite:
                                                                                '${schoolRepo.getSchoolModel.schoolList![v].website}',
                                                                            city:
                                                                                '${schoolRepo.getSchoolModel.schoolList![v].city}',
                                                                            pinCode:
                                                                                '${schoolRepo.getSchoolModel.schoolList![v].pinCode}',
                                                                            address:
                                                                                '${schoolRepo.getSchoolModel.schoolList![v].address}',
                                                                            email:
                                                                                '${schoolRepo.getSchoolModel.schoolList![v].email}',
                                                                            phoneNumber:
                                                                                '${schoolRepo.getSchoolModel.schoolList![v].phoneNumber}',
                                                                            schoolName:
                                                                                '${schoolRepo.getSchoolModel.schoolList![v].name}',
                                                                          )));
                                                        },
                                                        child: Container(
                                                          height: 30,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width /
                                                              3,
                                                          decoration: BoxDecoration(
                                                              color: const Color(
                                                                  0xffECFCFF),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5)),
                                                          child: const Center(
                                                            child: Text(
                                                              "Details",
                                                              textScaleFactor:
                                                                  0.85,
                                                              style: TextStyle(
                                                                  color: Color(
                                                                      0xff0E99AF),
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700),
                                                            ),
                                                          ),
                                                        ),
                                                      ),

                                                      // GestureDetector(
                                                      //   onTap: () async {
                                                      //     await _showMyDialog(
                                                      //         schoolRepo
                                                      //             .getSchoolModel
                                                      //             .schoolList![
                                                      //                 v]
                                                      //             .schoolId);
                                                      //   },
                                                      //   child: Container(
                                                      //     height: 30,
                                                      //     width: w * 0.25,
                                                      //     decoration: BoxDecoration(
                                                      //         color: const Color(
                                                      //             0xffFFF3F0),
                                                      //         borderRadius:
                                                      //             BorderRadius
                                                      //                 .circular(
                                                      //                     5)),
                                                      //     child: const Center(
                                                      //       child: Text(
                                                      //         "Delete",
                                                      //         textScaleFactor:
                                                      //             0.85,
                                                      //         style: TextStyle(
                                                      //             color: Color(
                                                      //                 0xffFF8B66),
                                                      //             fontSize: 16,
                                                      //             fontWeight:
                                                      //                 FontWeight
                                                      //                     .w700),
                                                      //       ),
                                                      //     ),
                                                      //   ),
                                                      // ),
                                                    ],
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 30,
                                      ),
                                    ],
                                  );
                                });
                      })
            ],
          ),
        ),
      ),
    );
  }
}

// class SchoolListCard extends StatelessWidget {
//   const SchoolListCard({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     double h = MediaQuery.of(context).size.height;
//     double w = MediaQuery.of(context).size.width;
//     return Container(
//       height: h * 0.3,
//       width: w,
//       decoration: BoxDecoration(
//         color: Colors.white,
//         borderRadius: BorderRadius.circular(7),
//         boxShadow: const [
//           BoxShadow(
//               color: Colors.grey,
//               offset: Offset(0, 2),
//               blurRadius: 20,
//               spreadRadius: 0)
//         ],
//       ),
//       child: Container(
//         margin: const EdgeInsets.only(left: 5,right: 3),
//         child: Row(
//           children: [
//             SizedBox(
//               height: h * 0.2,
//               width: 128,
//               child: Image.asset('assets/images/4.jpg',fit: BoxFit.cover,),
//             ),
//             const SizedBox(width: 20.0,),
//             Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 const Text(
//                   "School Name",
//                   style: TextStyle(
//                       color: Colors.black,
//                       fontSize: 25,
//                       fontWeight: FontWeight.w800),
//                 ),
//                 const SizedBox(
//                   height: 15.0,
//                 ),
//                 const Text(
//                   "Address of School",
//                   style: TextStyle(
//                       color: Colors.black,
//                       fontSize: 16,
//                       fontWeight: FontWeight.w600),
//                 ),
//                 const SizedBox(
//                   height: 10.0,
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     GestureDetector(
//                       onTap: (){
//                         Navigator.push(context, MaterialPageRoute(builder: (context)=> SchoolSurvey()));
//                       },
//                       child: Container(
//                         height: 40,
//                         width: w * 0.23,
//                         decoration: BoxDecoration(
//                             color: const Color(0xff634DD1),
//                             borderRadius: BorderRadius.circular(5)),
//                         child: const Center(
//                           child: Text(
//                             "Add Survey",
//                             textScaleFactor: 0.85,
//                             style: TextStyle(
//                                 color: Colors.white,
//                                 fontSize: 16,
//                                 fontWeight: FontWeight.w700),
//                           ),
//                         ),
//                       ),
//                     ),
//                     const SizedBox(
//                       width: 15.0,
//                     ),
//                     Container(
//                       height: 40,
//                       width: w * 0.23,
//                       decoration: BoxDecoration(
//                           color: const Color(0xff5CDD78),
//                           borderRadius: BorderRadius.circular(5)),
//                       child: const Center(
//                         child: Text(
//                           "Edit Survey",
//                           textScaleFactor: 0.85,
//                           style: TextStyle(
//                               color: Colors.white,
//                               fontSize: 16,
//                               fontWeight: FontWeight.w700),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//                 const SizedBox(
//                   height: 15.0,
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Container(
//                       height: 30,
//                       width: 60,
//                       decoration: BoxDecoration(
//                           color: const Color(0xffF4F3FC),
//                           borderRadius: BorderRadius.circular(5)),
//                       child: const Center(
//                         child: Text(
//                           "Edit ",
//                           textScaleFactor: 0.85,
//                           style: TextStyle(
//                               color: Color(0xff634DD1),
//                               fontSize: 16,
//                               fontWeight: FontWeight.w700),
//                         ),
//                       ),
//                     ),
//                     const SizedBox(
//                       width: 10.0,
//                     ),
//                     Container(
//                       height: 30,
//                       width: 60,
//                       decoration: BoxDecoration(
//                           color: const Color(0xffECFCFF),
//                           borderRadius: BorderRadius.circular(5)),
//                       child: const Center(
//                         child: Text(
//                           "Details",
//                           textScaleFactor: 0.85,
//                           style: TextStyle(
//                               color: Color(0xff0E99AF),
//                               fontSize: 16,
//                               fontWeight: FontWeight.w700),
//                         ),
//                       ),
//                     ),
//                     const SizedBox(
//                       width: 10.0,
//                     ),
//                     Container(
//                       height: 30,
//                       width: 60,
//                       decoration: BoxDecoration(
//                           color: const Color(0xffFFF3F0),
//                           borderRadius: BorderRadius.circular(5)),
//                       child: const Center(
//                         child: Text(
//                           "Delete",
//                           textScaleFactor: 0.85,
//                           style: TextStyle(
//                               color: Color(0xffFF8B66),
//                               fontSize: 16,
//                               fontWeight: FontWeight.w700),
//                         ),
//                       ),
//                     ),
//                   ],
//                 )
//               ],
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
