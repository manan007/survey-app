import 'package:flutter/material.dart';

import '../helper/const.dart';

class SchoolDetailsShow extends StatefulWidget {
  String? schoolName;
  String? address;
  String? phoneNumber;
  String? email;
  String? webSite;
  String? city;
  String? pinCode;

  SchoolDetailsShow({
    Key? key,
    @required this.schoolName,
    @required this.address,
    @required this.phoneNumber,
    @required this.email,
    @required this.webSite,
    @required this.city,
    @required this.pinCode,
  }) : super(key: key);

  @override
  _SchoolDetailsShowState createState() => _SchoolDetailsShowState();
}

class _SchoolDetailsShowState extends State<SchoolDetailsShow> {
  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: kWhiteColor,
        appBar: AppBar(
          leading: Builder(
              builder: (context) => IconButton(
                    onPressed: () {
                      setState(() {
                        Navigator.pop(context);
                      });
                    },
                    icon: Icon(Icons.arrow_back),
                    color: Colors.black,
                  )),
          backgroundColor: Colors.white,
          title: Container(
              height: 70,
              width: double.infinity,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage('assets/images/Band 04 copy.png'))),
              child: Center(child: Text("School Details"))),
        ),
        body: Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 15),
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Card(
              shadowColor: Colors.deepPurple,
              elevation: 10.0,
              child: Container(
                padding: const EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 30.0,
                    ),
                    Row(
                      children: [
                        const Text("School Name:",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w800)),
                        const SizedBox(
                          width: 10.0,
                        ),
                        FittedBox(
                          child: Text("${widget.schoolName}",
                              textScaleFactor: 0.85,
                              style: const TextStyle(
                                  color: Colors.deepPurple,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w800)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      children: [
                        const Text("Address:",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w800)),
                        const SizedBox(
                          width: 10.0,
                        ),
                        Container(
                          width: w * 0.6,
                          child: Text("${widget.address}",
                              textScaleFactor: 0.85,
                              style: const TextStyle(
                                  color: Colors.deepPurple,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w800)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      children: [
                        const Text("Phone Number:",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w800)),
                        const SizedBox(
                          width: 10.0,
                        ),
                        FittedBox(
                          child: Text("${widget.phoneNumber}",
                              textScaleFactor: 0.85,
                              style: const TextStyle(
                                  color: Colors.deepPurple,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w800)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      children: [
                        const Text("Email:",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w800)),
                        const SizedBox(
                          width: 10.0,
                        ),
                        FittedBox(
                          child: Text("${widget.email}",
                              textScaleFactor: 0.85,
                              style: const TextStyle(
                                  color: Colors.deepPurple,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w800)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      children: [
                        const Text("Website:",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w800)),
                        const SizedBox(
                          width: 10.0,
                        ),
                        FittedBox(
                          child: Text("${widget.webSite}",
                              textScaleFactor: 0.85,
                              style: const TextStyle(
                                  color: Colors.deepPurple,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w800)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      children: [
                        const Text("City:",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w800)),
                        const SizedBox(
                          width: 10.0,
                        ),
                        FittedBox(
                          child: Text("${widget.city}",
                              textScaleFactor: 0.85,
                              style: const TextStyle(
                                  color: Colors.deepPurple,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w800)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      children: [
                        const Text("PinCode:",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w800)),
                        const SizedBox(
                          width: 10.0,
                        ),
                        FittedBox(
                          child: Text("${widget.pinCode}",
                              textScaleFactor: 0.85,
                              style: const TextStyle(
                                  color: Colors.deepPurple,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w800)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
