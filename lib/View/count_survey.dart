import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:survey/helper/const.dart';

class CountSurvey extends StatefulWidget {
  const CountSurvey({Key? key}) : super(key: key);

  @override
  _CountSurveyState createState() => _CountSurveyState();
}

class _CountSurveyState extends State<CountSurvey> {
  ValueNotifier<String> radioItem = ValueNotifier('');

  ValueNotifier<String> dryWaste = ValueNotifier('');

  String _image = "";
  final ImagePicker _picker = ImagePicker();

  Future selectImageFromGallery(quality) async {
    final XFile? image = await _picker.pickImage(
        source: ImageSource.gallery, imageQuality: quality);
    if (image == null) {
      return "";
    } else {
      return image.path;
    }
  }

  Future selectImageFromCamera(quality) async {
    final XFile? image = await _picker.pickImage(
        source: ImageSource.camera, imageQuality: quality);
    if (image == null) {
      return "";
    } else {
      return image.path;
    }
  }

  void checkFieldEmptyOrNot() async {
    if (radioItem.value == '') {
      Fluttertoast.showToast(msg: "Please Select Wet waste");
    } else if (dryWaste.value == '') {
      Fluttertoast.showToast(msg: "Please Select Dry waste");
    } else if (_image == "") {
      Fluttertoast.showToast(msg: "Please Upload Photos of Bins");
    }
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        leading: Builder(
            builder: (context) => IconButton(
                  onPressed: () {
                    setState(() {
                      Navigator.pop(context);
                    });
                  },
                  icon: Icon(Icons.arrow_back),
                  color: Colors.black,
                )),
        backgroundColor: Colors.white,
        title: Container(
            height: 70,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/Band 04 copy.png'))),
            child: Center(child: Text("Count.. Survey"))),
      ),
      body: Container(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: h * 0.03,
              ),
              const Text(
                "Garbage management",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600),
              ),
              Row(
                children: [
                  const Text(
                    "Wet Waste",
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                  ValueListenableBuilder(
                      valueListenable: radioItem,
                      builder: (context, v, c) {
                        return Radio(
                          groupValue: radioItem.value,
                          activeColor: Colors.deepPurple,
                          value: 'Item 1',
                          onChanged: (val) {
                            radioItem.value = val.toString();
                          },
                        );
                      }),
                  const Text("Yes"),
                  ValueListenableBuilder(
                      valueListenable: radioItem,
                      builder: (context, v, c) {
                        return Radio(
                          groupValue: radioItem.value,
                          activeColor: Colors.deepPurple,
                          value: 'Item 2',
                          onChanged: (val) {
                            radioItem.value = val.toString();
                          },
                        );
                      }),
                  const Text("No"),
                ],
              ),
              Row(
                children: [
                  const Text(
                    "Dry Waste",
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                  ValueListenableBuilder(
                      valueListenable: dryWaste,
                      builder: (context, v, c) {
                        return Radio(
                          groupValue: dryWaste.value,
                          activeColor: Colors.deepPurple,
                          value: 'Item 1',
                          onChanged: (val) {
                            dryWaste.value = val.toString();
                          },
                        );
                      }),
                  const Text("Yes"),
                  ValueListenableBuilder(
                      valueListenable: dryWaste,
                      builder: (context, v, c) {
                        return Radio(
                          groupValue: dryWaste.value,
                          activeColor: Colors.deepPurple,
                          value: 'Item 2',
                          onChanged: (val) {
                            dryWaste.value = val.toString();
                          },
                        );
                      }),
                  const Text("No"),
                ],
              ),
              const SizedBox(
                height: 20.0,
              ),
              Container(
                height: h * 0.4,
                width: w,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(7),
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0, 2),
                        blurRadius: 20,
                        spreadRadius: 0)
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 15, top: 15),
                      child: const Text(
                        "Photos of Bins",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    Center(
                      child: DottedBorder(
                        strokeWidth: 3.0,
                        padding: const EdgeInsets.all(10.0),
                        borderType: BorderType.Rect,
                        color: Colors.grey,
                        child: _image != ""
                            ? Image.file(
                                File(_image),
                                height: h * 0.3,
                                width: w * 0.8,
                                fit: BoxFit.cover,
                              )
                            : GestureDetector(
                                onTap: () async {
                                  _image = await selectImageFromCamera(30);
                                  setState(() {});
                                },
                                child: Container(
                                  height: h * 0.3,
                                  width: w * 0.8,
                                  color: const Color(0xffFAFAFC),
                                  child: Column(
                                    children: const [
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Text(
                                        "Drop files here",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Text(
                                        "Support Formate : PNG ,JPG",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                        ),
                                      ),
                                      Text(
                                        "OR",
                                        style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 20,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Text(
                                        "Browse Files",
                                        style: TextStyle(
                                          color: Colors.deepPurple,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              const Text(
                "Add More Question For Survey....",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: Colors.black),
              ),
              const SizedBox(
                height: 15.0,
              ),
              Container(
                height: 50,
                width: w,
                decoration: BoxDecoration(
                    color: Colors.deepPurple,
                    borderRadius: BorderRadius.circular(10)),
                child: const Center(
                  child: Text(
                    "Submit",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 15.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
