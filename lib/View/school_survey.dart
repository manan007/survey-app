import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:survey/helper/const.dart';

class SchoolSurvey extends StatefulWidget {
  // int? schoolID;
  //
  // SchoolSurvey(this.schoolID);

  @override
  _SchoolSurveyState createState() => _SchoolSurveyState();
}

class _SchoolSurveyState extends State<SchoolSurvey> {
  ValueNotifier<String> dropdownvalue = ValueNotifier('Yes');

  TextEditingController? howManyGirlsAndBoys = TextEditingController();
  TextEditingController? howManyClassroom = TextEditingController();
  TextEditingController? doesThisSchoolCompost = TextEditingController();

  ValueNotifier<String> radioItem = ValueNotifier('');

  ValueNotifier<String> dryWaste = ValueNotifier('');

  var items = ['Yes', 'No'];

  String _image = "";
  final ImagePicker _picker = ImagePicker();
  bool isApiCalling = false;

  Future selectImageFromGallery(quality) async {
    final XFile? image = await _picker.pickImage(
        source: ImageSource.gallery, imageQuality: quality);
    if (image == null) {
      return "";
    } else {
      return image.path;
    }
  }

  Future selectImageFromCamera(quality) async {
    final XFile? image = await _picker.pickImage(
        source: ImageSource.camera, imageQuality: quality);
    if (image == null) {
      return "";
    } else {
      return image.path;
    }
  }

  void checkTextFieldEmptyOrNot() {
    if (howManyGirlsAndBoys!.text == "") {
      Fluttertoast.showToast(
          msg: "Please Enter How many girls and boys enrolled");
    } else if (howManyClassroom!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter How many classrooms");
    } else if (doesThisSchoolCompost!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter Does this compost");
    } else if (_image == "") {
      Fluttertoast.showToast(msg: "Please Upload ClassRoom Image");
    } else if (radioItem.value == '') {
      Fluttertoast.showToast(msg: "Please Select Wet waste");
    } else if (dryWaste.value == '') {
      Fluttertoast.showToast(msg: "Please Select Dry waste");
    } else {
      Fluttertoast.showToast(msg: "success");
    }
  }

  @override
  void dispose() {
    howManyGirlsAndBoys!.dispose();
    howManyClassroom!.dispose();
    doesThisSchoolCompost!.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        leading: Builder(
            builder: (context) => IconButton(
                  onPressed: () {
                    setState(() {
                      Navigator.pop(context);
                    });
                  },
                  icon: Icon(Icons.arrow_back),
                  color: Colors.black,
                )),
        backgroundColor: Colors.white,
        title: Container(
            height: 70,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/Band 04 copy.png'))),
            child: Center(child: Text("School Survey"))),
      ),
      body: Container(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: h * 0.03,
              ),
              //Name Of School
              const Text(
                "How many girls and boys enrolled",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600),
              ),
              TextField(
                controller: howManyGirlsAndBoys,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.grey, width: 1.0),
                      borderRadius: BorderRadius.circular(7)),
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Color(0xff634DD1), width: 1.0),
                    borderRadius: BorderRadius.circular(7),
                  ),
                ),
              ),
              //Address Part
              const SizedBox(
                height: 15.0,
              ),
              const Text(
                "How many classrooms",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600),
              ),
              TextField(
                controller: howManyClassroom,
                maxLines: 3,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.grey, width: 1.0),
                      borderRadius: BorderRadius.circular(7)),
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Color(0xff634DD1), width: 1.0),
                    borderRadius: BorderRadius.circular(7),
                  ),
                ),
              ),
              //Medium Of School
              const SizedBox(
                height: 15.0,
              ),
              const Text(
                "Solar or no Solar",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600),
              ),
              ValueListenableBuilder(
                  valueListenable: dropdownvalue,
                  builder: (context, v, c) {
                    return Container(
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.grey),
                      ),
                      child: DropdownButton(
                        // Initial Value
                        value: dropdownvalue.value,
                        underline: Container(
                          height: 3,
                          width: w,
                          color: Colors.transparent,
                        ),
                        // Down Arrow Icon
                        icon: const Icon(Icons.keyboard_arrow_down),
                        isExpanded: true,
                        // Array list of items
                        items: items.map((String items) {
                          return DropdownMenuItem(
                            value: items,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(items),
                            ),
                          );
                        }).toList(),
                        onChanged: (String? newValue) {
                          dropdownvalue.value = newValue!;
                        },
                      ),
                    );
                  }),
              //Gps Tag Of School
              const SizedBox(
                height: 15.0,
              ),
              const Text(
                "Does this School compost",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600),
              ),
              TextField(
                controller: doesThisSchoolCompost,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.grey, width: 1.0),
                      borderRadius: BorderRadius.circular(7)),
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Color(0xff634DD1), width: 1.0),
                    borderRadius: BorderRadius.circular(7),
                  ),
                ),
              ),

              const SizedBox(
                height: 15.0,
              ),
              Container(
                height: h * 0.4,
                width: w,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(7),
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0, 2),
                        blurRadius: 20,
                        spreadRadius: 0)
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 15, top: 15),
                      child: const Text(
                        "Photos of Classroom",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    Center(
                      child: DottedBorder(
                        strokeWidth: 3.0,
                        padding: const EdgeInsets.all(10.0),
                        borderType: BorderType.Rect,
                        color: Colors.grey,
                        child: _image != ""
                            ? GestureDetector(
                                onTap: () {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        double h =
                                            MediaQuery.of(context).size.height;
                                        double w =
                                            MediaQuery.of(context).size.width;

                                        return Dialog(
                                          backgroundColor:
                                              const Color(0xc7f9f9f9),
                                          child: Container(
                                            height: h * 0.12,
                                            width: w * 0.35,
                                            decoration: BoxDecoration(
                                              color: const Color(0xc7f9f9f9),
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Expanded(
                                                    child: InkWell(
                                                      onTap: () async {
                                                        _image =
                                                            await selectImageFromCamera(
                                                                30);
                                                        setState(() {});
                                                        Navigator.pop(context);
                                                      },
                                                      splashColor: Colors.grey
                                                          .withOpacity(0.7),
                                                      child: Container(
                                                        color:
                                                            Colors.transparent,
                                                        child: const Center(
                                                          child: Text(
                                                            "Camera",
                                                            style: TextStyle(
                                                                color: Color(
                                                                    0xff0089f1),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                fontFamily:
                                                                    'Raleway',
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontSize: 17.0),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  const Divider(
                                                    height: 0.0,
                                                    color: Colors.grey,
                                                  ),
                                                  Expanded(
                                                    child: InkWell(
                                                      onTap: () async {
                                                        _image =
                                                            await selectImageFromGallery(
                                                                30);
                                                        setState(() {});
                                                        Navigator.pop(context);
                                                      },
                                                      splashColor: Colors.grey
                                                          .withOpacity(0.7),
                                                      child: Container(
                                                        color:
                                                            Colors.transparent,
                                                        child: const Center(
                                                          child: Text(
                                                            "Gallery",
                                                            style: TextStyle(
                                                                color: Color(
                                                                    0xff0089f1),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                fontFamily:
                                                                    'Raleway',
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontSize: 17.0),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      });
                                },
                                child: Image.file(
                                  File(_image),
                                  height: h * 0.3,
                                  width: w * 0.8,
                                  fit: BoxFit.cover,
                                ),
                              )
                            : GestureDetector(
                                onTap: () async {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        double h =
                                            MediaQuery.of(context).size.height;
                                        double w =
                                            MediaQuery.of(context).size.width;

                                        return Dialog(
                                          backgroundColor:
                                              const Color(0xc7f9f9f9),
                                          child: Container(
                                            height: h * 0.12,
                                            width: w * 0.35,
                                            decoration: BoxDecoration(
                                              color: const Color(0xc7f9f9f9),
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Expanded(
                                                    child: InkWell(
                                                      onTap: () async {
                                                        _image =
                                                            await selectImageFromCamera(
                                                                30);
                                                        setState(() {});
                                                        Navigator.pop(context);
                                                      },
                                                      splashColor: Colors.grey
                                                          .withOpacity(0.7),
                                                      child: Container(
                                                        color:
                                                            Colors.transparent,
                                                        child: const Center(
                                                          child: Text(
                                                            "Camera",
                                                            style: TextStyle(
                                                                color: Color(
                                                                    0xff0089f1),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                fontFamily:
                                                                    'Raleway',
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontSize: 17.0),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  const Divider(
                                                    height: 0.0,
                                                    color: Colors.grey,
                                                  ),
                                                  Expanded(
                                                    child: InkWell(
                                                      onTap: () async {
                                                        _image =
                                                            await selectImageFromGallery(
                                                                30);
                                                        setState(() {});
                                                        Navigator.pop(context);
                                                      },
                                                      splashColor: Colors.grey
                                                          .withOpacity(0.7),
                                                      child: Container(
                                                        color:
                                                            Colors.transparent,
                                                        child: const Center(
                                                          child: Text(
                                                            "Gallery",
                                                            style: TextStyle(
                                                                color: Color(
                                                                    0xff0089f1),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                fontFamily:
                                                                    'Raleway',
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontSize: 17.0),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      });
                                },
                                child: Container(
                                  height: h * 0.3,
                                  width: w * 0.8,
                                  color: const Color(0xffFAFAFC),
                                  child: Column(
                                    children: const [
                                      SizedBox(
                                        height: 20,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 15.0,
              ),

              const Text(
                "Garbage management",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600),
              ),
              Row(
                children: [
                  const Text(
                    "Wet Waste",
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                  ValueListenableBuilder(
                      valueListenable: radioItem,
                      builder: (context, v, c) {
                        return Radio(
                          groupValue: radioItem.value,
                          activeColor: Colors.deepPurple,
                          value: 'Item 1',
                          onChanged: (val) {
                            radioItem.value = val.toString();
                          },
                        );
                      }),
                  const Text("Yes"),
                  ValueListenableBuilder(
                      valueListenable: radioItem,
                      builder: (context, v, c) {
                        return Radio(
                          groupValue: radioItem.value,
                          activeColor: Colors.deepPurple,
                          value: 'Item 2',
                          onChanged: (val) {
                            radioItem.value = val.toString();
                          },
                        );
                      }),
                  const Text("No"),
                ],
              ),
              Row(
                children: [
                  const Text(
                    "Dry Waste",
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                  ValueListenableBuilder(
                      valueListenable: dryWaste,
                      builder: (context, v, c) {
                        return Radio(
                          groupValue: dryWaste.value,
                          activeColor: Colors.deepPurple,
                          value: 'Item 1',
                          onChanged: (val) {
                            dryWaste.value = val.toString();
                          },
                        );
                      }),
                  const Text("Yes"),
                  ValueListenableBuilder(
                      valueListenable: dryWaste,
                      builder: (context, v, c) {
                        return Radio(
                          groupValue: dryWaste.value,
                          activeColor: Colors.deepPurple,
                          value: 'Item 2',
                          onChanged: (val) {
                            dryWaste.value = val.toString();
                          },
                        );
                      }),
                  const Text("No"),
                ],
              ),
              const SizedBox(
                height: 20.0,
              ),

              const SizedBox(
                height: 15.0,
              ),
              GestureDetector(
                onTap: () {
                  checkTextFieldEmptyOrNot();
                },
                child: Center(
                  child: Container(
                    height: 50,
                    width: w * 0.3,
                    decoration: BoxDecoration(
                        color: Colors.deepPurple,
                        borderRadius: BorderRadius.circular(10)),
                    child: const Center(
                      child: Text(
                        "School Survey",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                            fontSize: 16.0),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  getData() async {
    setState(() {
      isApiCalling = true;
    });
    // bool result = await schoolRepo.addSchoolSurvey(widget.schoolID);
    // if (result) {}
    setState(() {
      isApiCalling = false;
    });
  }
}
