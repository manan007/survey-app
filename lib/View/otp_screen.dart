import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:survey/View/dashbord.dart';
import 'package:survey/View/profile.dart';
import 'package:survey/helper/const.dart';
import 'package:survey/helper/global_variable.dart';

class OtpScreen extends StatefulWidget {
  String? emailAddress;

  OtpScreen({Key? key, required this.emailAddress}) : super(key: key);

  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  late bool passwordVisible;

  ValueNotifier<bool> isApiCall = ValueNotifier(false);

  TextEditingController? otpNumber = TextEditingController();

  late Timer timer;
  int timerMaxSeconds = 30;

  int currentSeconds = 0;

  String get timerText =>
      ((timerMaxSeconds - currentSeconds) % 60).toString().padLeft(0, '');

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (timerMaxSeconds == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            timerMaxSeconds--;
          });
        }
      },
    );
  }

  ValueNotifier<bool> timerProgress = ValueNotifier(false);

  void checkOtpFieldEmptyOrNot() async {
    isApiCall.value = true;
    if (otpNumber!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter OTP digits");
    } else if (otpNumber!.text.length != 6) {
      Fluttertoast.showToast(msg: "Please Enter 6 digit OTP digits");
    } else if (otpNumber!.text != storeOtpNumber.value.toString()) {
      Fluttertoast.showToast(msg: "Please Enter Valid OTP");
    } else {
      bool result = await serverAuth.otpVerification(
          int.parse(otpNumber!.text), widget.emailAddress!);
      print(result);
      if (result) {
        SharedPreferences preferences = await SharedPreferences.getInstance();
        if (preferences.getBool('isNew')!) {
          Navigator.pushAndRemoveUntil<void>(
            context,
            MaterialPageRoute<void>(
              builder: (BuildContext context) => const Profile(),
            ),
            (Route<dynamic> route) => false,
          );
        } else {
          Navigator.pushAndRemoveUntil<void>(
            context,
            MaterialPageRoute<void>(
              builder: (BuildContext context) => const DashBord(),
            ),
            (Route<dynamic> route) => false,
          );
        }
      } else {
        Fluttertoast.showToast(msg: "Please Resend OTP");
      }
    }
    isApiCall.value = false;
  }

  @override
  void initState() {
    startTimer();
    passwordVisible = true;
    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: kWhiteColor,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: h * 0.3,
              width: w,
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
                child: Image.asset(
                  'assets/images/good food newsroom.jpg',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: const Text(
                "Enter OTP Sent to Your Email Address",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600),
              ),
            ),
            const SizedBox(
              height: 12.0,
            ),
            Container(
              padding: const EdgeInsets.only(left: 18.0, right: 18.0),
              child: PinCodeTextField(
                appContext: context,
                length: 6,
                controller: otpNumber,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                ],
                obscureText: false,
                animationType: AnimationType.fade,
                animationDuration: const Duration(milliseconds: 300),
                onChanged: (value) {},
                pinTheme: PinTheme(
                    shape: PinCodeFieldShape.box,
                    borderRadius: BorderRadius.circular(5),
                    activeColor: Colors.purple,
                    inactiveColor: Colors.purple,
                    selectedColor: Colors.purple),
                onCompleted: (val) {},
              ),
            ),
            const SizedBox(
              height: 12.0,
            ),
            timerMaxSeconds == 0
                ? ValueListenableBuilder(
                    valueListenable: timerProgress,
                    builder: (context, v, c) {
                      return timerProgress.value == true
                          ? const Center(child: CircularProgressIndicator())
                          : GestureDetector(
                              onTap: () async {
                                timerProgress.value = true;
                                await serverAuth
                                    .emailLogin(widget.emailAddress);
                                setState(() {
                                  otpNumber!.clear();
                                  startTimer();
                                  timerMaxSeconds = 30;
                                  timerProgress.value = false;
                                });
                              },
                              child: Center(
                                child: Container(
                                  margin: const EdgeInsets.only(right: 15),
                                  height: 50,
                                  width: w * 0.3,
                                  child: const Center(
                                    child: Text(
                                      "Resend OTP?",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.deepPurple),
                                    ),
                                  ),
                                ),
                              ),
                            );
                    })
                : Center(
                    child: Container(
                      height: 50,
                      margin: const EdgeInsets.only(right: 15),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          " Resend OTP in $timerText sec",
                          style: const TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w700,
                              color: Colors.grey),
                        ),
                      ),
                    ),
                  ),
            const SizedBox(
              height: 30.0,
            ),
            ValueListenableBuilder(
                valueListenable: isApiCall,
                builder: (context, v, c) {
                  return GestureDetector(
                    onTap: () {
                      if (isApiCall.value == false) {
                        checkOtpFieldEmptyOrNot();
                      }
                    },
                    child: Container(
                      margin: const EdgeInsets.only(left: 15, right: 15),
                      height: 50,
                      width: w,
                      decoration: BoxDecoration(
                          color: backGroundColor,
                          borderRadius: BorderRadius.circular(10)),
                      child: isApiCall.value == true
                          ? Center(
                              child: CircularProgressIndicator(
                              color: kWhiteColor,
                            ))
                          : const Center(
                              child: Text(
                                "Sign In",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }
}
