import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:survey/View/otp_screen.dart';
import 'package:survey/helper/const.dart';
import 'package:survey/helper/global_variable.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController? email = TextEditingController();

  ValueNotifier<bool> isApiCall = ValueNotifier(false);

  void checkTextFieldEmptyOrNot() async {
    if (email!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter Email Address");
    } else {
      isApiCall.value = true;
      bool result = await serverAuth.emailLogin(email!.text);
      if (result) {
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.setString("emailAddress", email!.text);
        Navigator.pushAndRemoveUntil<void>(
          context,
          MaterialPageRoute<void>(
            builder: (BuildContext context) => OtpScreen(
              emailAddress: email!.text,
            ),
          ),
          (Route<dynamic> route) => false,
        );

        isApiCall.value = false;
      } else {
        Fluttertoast.showToast(msg: "Internal server Error");
        isApiCall.value = false;
      }
    }
  }

  @override
  void dispose() {
    email!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: kWhiteColor,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: h * 0.3,
              width: w,
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
                child: Image.asset(
                  'assets/images/cycling cities newsroom.jpg',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(
              height: 13.0,
            ),
            const SizedBox(
              height: 10.0,
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: const Text(
                "Hello,",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 30,
                    fontWeight: FontWeight.w800),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: const Text(
                "Welcome",
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 20,
                ),
              ),
            ),
            const SizedBox(
              height: 12.0,
            ),
            const SizedBox(
              height: 12.0,
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: TextField(
                keyboardType: TextInputType.emailAddress,
                controller: email,
                decoration: InputDecoration(
                  fillColor: Colors.grey[300],
                  hintText: "Enter Your Email",
                  filled: true,
                  prefixIcon: const Icon(
                    CupertinoIcons.mail,
                    size: 30,
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 30.0,
            ),
            ValueListenableBuilder(
                valueListenable: isApiCall,
                builder: (context, v, c) {
                  return isApiCall.value == true
                      ? Container(
                          margin: const EdgeInsets.only(left: 15, right: 15),
                          height: 50,
                          width: w,
                          decoration: BoxDecoration(
                              color: const Color(0xff634ED1),
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                              child: CircularProgressIndicator(
                            color: kWhiteColor,
                          )),
                        )
                      : GestureDetector(
                          onTap: () {
                            checkTextFieldEmptyOrNot();
                          },
                          child: Container(
                            margin: const EdgeInsets.only(left: 15, right: 15),
                            height: 50,
                            width: w,
                            decoration: BoxDecoration(
                                color: backGroundColor,
                                borderRadius: BorderRadius.circular(10)),
                            child: const Center(
                              child: Text(
                                "Send OTP",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                        );
                })
          ],
        ),
      ),
    );
  }
}
