import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:survey/helper/global_variable.dart';
import 'package:survey/model/Add_School_Survey_Model.dart';
import 'package:survey/model/radioButtonControler.dart';
import 'package:survey/model/switchRadioButtonController.dart';

class AddSchoolUI extends StatefulWidget {
  @override
  _AddSchoolUIState createState() => _AddSchoolUIState();
}

class _AddSchoolUIState extends State<AddSchoolUI> {
  late List<SurveyQuestions> uiModel = [];
  var textEditingControllers = <TextEditingController>[];

  var checkBoxEditingControllers = <TextEditingController>[];

  var radioArr = <radioButtonController>[];
  var switchRadioArr = List<switchRadioButtonController>;

  var textFields = <TextField>[];
  List<Widget> randomWidgetsArr = const <Widget>[];

  Future<AddSchoolUiModel> initialize() async {
    http.Response response =
        await http.post(Uri.parse("$baseUrl/Survey/AddSchoolSurvey"),
            headers: {
              "content-type": "application/json",
              "Accept": "application/json",
            },
            body: convert.jsonEncode({
              "SchoolId": 1,
            }));
    // print(response.body);
    var data = convert.jsonDecode(response.body);

    var parsedJsonData = data['Survey_Questions'];
    print("modelclass" + data['Survey_Questions'].toString());
    // List<AddSchoolUiModel> uiModel = AddSchoolUiModel.fromJson(parsedJsonData).toList();

    return data['Survey_Questions']
        .map((e) => AddSchoolUiModel.fromJson(e))
        .toList();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initialize().then((value) {
      setState(() {
        uiModel = value as List<SurveyQuestions>;
      });
      addControllers();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: randomWidgetsArr,
    ));
  }

  void addControllers() {
    for (int i = 0; i < uiModel.length; i++) {
      if (uiModel[i].controlTypeId!.controlTypeId == 1) {
        print("controlTYpeID" +
            uiModel[i].controlTypeId!.controlTypeId.toString());
        var textEditingController =
            TextEditingController(text: uiModel[i].controlTypeId!.controlType);
        textEditingControllers.add(textEditingController);
        randomWidgetsArr.add(TextField(
          controller: textEditingController,
        ));
      } else if (uiModel[i].controlTypeId!.controlTypeId == 2) {
      } else if (uiModel[i].controlTypeId!.controlTypeId == 3) {
      } else if (uiModel[i].controlTypeId!.controlTypeId == 4) {
        randomWidgetsArr.add(Column(
          children: [
            Text(uiModel[i].question!),
            ...uiModel[i].option!.map((e) => Row(
                  children: [
                    Text(e.option!),
                    Radio<String>(
                      value: (convert.jsonEncode(radioButtonController(
                          uiModel[i].questionId, e.optionId))),
                      groupValue: convert.jsonEncode(uiModel[i].questionId),
                      onChanged: (String? value) async {
                        radioButtonController controller =
                            convert.jsonDecode(value!);
                        setState(() {
                          if (radioArr.contains(controller)) {
                            radioArr.remove(controller);
                          } else {
                            radioArr.add(controller);
                          }
                        });
                      },
                    )
                  ],
                ))
          ],
        ));
      } else if (uiModel[i].controlTypeId!.controlTypeId == 5) {
      } else if (uiModel[i].controlTypeId!.controlTypeId == 6) {}
    }
    print("textField" + textFields.length.toString());
  }
}
