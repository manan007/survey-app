import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:multiselect/multiselect.dart';
import 'package:survey/helper/const.dart';
import 'package:survey/helper/global_variable.dart';

class EditSchool extends StatefulWidget {
  String? schoolName;
  String? address;
  String? phoneNumber;
  String? email;
  String? webSite;
  int? schoolId;
  int? stateId;
  String? city;
  String? state;
  String? medium;
  String? Mediumids;
  String? pinCode;

  EditSchool(
      {Key? key,
      @required this.schoolId,
      @required this.schoolName,
      @required this.address,
      @required this.phoneNumber,
      @required this.email,
      @required this.webSite,
      @required this.city,
      @required this.stateId,
      @required this.pinCode,
      @required this.state,
      @required this.Mediumids,
      @required this.medium})
      : super(key: key);

  @override
  _EditSchoolState createState() => _EditSchoolState();
}

class _EditSchoolState extends State<EditSchool> {
  ValueNotifier<bool> addSchoolValueNotifier = ValueNotifier(false);

  TextEditingController? nameOfSchool = TextEditingController();
  TextEditingController? phoneNumber = TextEditingController();
  TextEditingController? email = TextEditingController();
  TextEditingController? website = TextEditingController();
  TextEditingController? stateId = TextEditingController();
  TextEditingController? city = TextEditingController();
  TextEditingController? address = TextEditingController();
  TextEditingController? pinCodeField = TextEditingController();

  void checkTextFieldEmptyOrNot() async {
    if (nameOfSchool!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter School Name");
    } else if (Address == "") {
      Fluttertoast.showToast(msg: "Please tap on Location");
    } else if (phoneNumber!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter phone Number");
    } else if (email!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter Email Id");
    } else if (website!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter Website");
    } else if (dropdownvalue == "") {
      Fluttertoast.showToast(msg: "Please Select State");
    } else if (city!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter city");
    } else if (pinCodeField!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter PinCode");
    } else if (mediumData == "") {
      Fluttertoast.showToast(msg: "Please Select Medium");
    } else {
      int? pincode = int.parse(pinCodeField!.text);
      addSchoolValueNotifier.value = true;
      // mediumData = "1,2";
      print(mediumData);
      bool result = await schoolRepo.editSchool(
          widget.schoolId,
          nameOfSchool!.text,
          Address,
          location,
          phoneNumber!.text,
          email!.text,
          website!.text,
          selectStateID,
          city!.text,
          pincode,
          mediumData,
          userId.value);

      if (result) {
        Navigator.pop(context, true);
        addSchoolValueNotifier.value = false;
        nameOfSchool!.clear();
        phoneNumber!.clear();
        email!.clear();
        website!.clear();
        city!.clear();
        pinCodeField!.clear();
      } else {
        Fluttertoast.showToast(msg: "Something Went Wrong");
        addSchoolValueNotifier.value = false;
      }
    }
  }

  String location = 'Null, Press Button';

  String Address = '';
  List<String> selected = [];
  String dropdownvalue = '';
  bool isApiCalling = false;
  String mediumDataNames = "";
  String mediumData = "";
  int? selectStateID = 0;

  Future<Position> _getGeoLocationPosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      await Geolocator.openLocationSettings();
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  Future<void> GetAddressFromLatLong(Position position) async {
    List<Placemark> placemarks =
        await placemarkFromCoordinates(position.latitude, position.longitude);
    print(placemarks);
    Placemark place = placemarks[0];
    Address =
        '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    address!.text =
        '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    setState(() {});
  }

  void getData() async {
    setState(() {
      isApiCalling = true;
    });
    await schoolRepo.getAllMedium();
    await schoolRepo.getAllState();
    setState(() {
      isApiCalling = false;
    });
  }

  @override
  void initState() {
    getData();
    nameOfSchool!.text = widget.schoolName.toString();
    Address = widget.address.toString();
    address!.text = widget.address.toString();
    phoneNumber!.text = widget.phoneNumber.toString();
    email!.text = widget.email.toString();
    website!.text = widget.webSite.toString();
    selectStateID = widget.stateId;
    dropdownvalue = widget.state.toString();
    mediumData = widget.Mediumids.toString();
    mediumDataNames = widget.medium.toString();
    city!.text = widget.city.toString();
    pinCodeField!.text = widget.pinCode.toString();
    super.initState();
  }

  @override
  void dispose() {
    nameOfSchool!.dispose();
    phoneNumber!.dispose();
    email!.dispose();
    website!.dispose();
    city!.dispose();
    pinCodeField!.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, false);
        return true;
      },
      child: Scaffold(
        backgroundColor: kWhiteColor,
        appBar: AppBar(
          leading: Builder(
              builder: (context) => IconButton(
                    onPressed: () {
                      setState(() {
                        Navigator.pop(context);
                      });
                    },
                    icon: Icon(Icons.arrow_back),
                    color: Colors.black,
                  )),
          backgroundColor: Colors.white,
          title: Container(
              height: 70,
              width: double.infinity,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage('assets/images/Band 04 copy.png'))),
              child: Center(child: Text("Edit School"))),
        ),
        body: isApiCalling
            ? const Center(child: CircularProgressIndicator())
            : Container(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: h * 0.03,
                      ),
                      //Name Of School
                      const Text(
                        "Name of School",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      TextField(
                        controller: nameOfSchool,
                        style: const TextStyle(height: 0.8),
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1.0),
                              borderRadius: BorderRadius.circular(7)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color(0xff634DD1), width: 1.0),
                            borderRadius: BorderRadius.circular(7),
                          ),
                        ),
                      ),

                      //Address Part
                      const SizedBox(
                        height: 10.0,
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            "Address",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                          IconButton(
                              onPressed: () async {
                                Position position =
                                    await _getGeoLocationPosition();
                                location =
                                    'Lat: ${position.latitude} , Long: ${position.longitude}';
                                GetAddressFromLatLong(position);
                              },
                              icon: Icon(
                                Icons.location_on_rounded,
                                size: 30,
                                color: backGroundColor,
                              )),
                        ],
                      ),
                      TextField(
                        controller: address,
                        style: const TextStyle(height: 1),
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1.0),
                              borderRadius: BorderRadius.circular(7)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color(0xff634DD1), width: 1.0),
                            borderRadius: BorderRadius.circular(7),
                          ),
                        ),
                      ),

                      const SizedBox(
                        height: 10.0,
                      ),
                      //Phone Number
                      const Text(
                        "Phone Number",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      TextField(
                        controller: phoneNumber,
                        style: const TextStyle(height: 0.8),
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1.0),
                              borderRadius: BorderRadius.circular(7)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color(0xff634DD1), width: 1.0),
                            borderRadius: BorderRadius.circular(7),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      //Email Part
                      const Text(
                        "Email",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      TextField(
                        controller: email,
                        style: const TextStyle(height: 0.8),
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1.0),
                              borderRadius: BorderRadius.circular(7)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color(0xff634DD1), width: 1.0),
                            borderRadius: BorderRadius.circular(7),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      //Website
                      const Text(
                        "Website",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      TextField(
                        controller: website,
                        style: const TextStyle(height: 0.8),
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1.0),
                              borderRadius: BorderRadius.circular(7)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color(0xff634DD1), width: 1.0),
                            borderRadius: BorderRadius.circular(7),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      const Text(
                        "State",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 50,
                        child: FormField(
                          builder: (FormFieldState<String> state) {
                            return InputDecorator(
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(8.0),
                                fillColor: Colors.white,
                                filled: true,
                                // hintText: "Your Phone Number",
                                enabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.grey, width: 1.0),
                                    borderRadius: BorderRadius.circular(7)),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Color(0xff634DD1), width: 1.0),
                                  borderRadius: BorderRadius.circular(7),
                                ),
                              ),
                              child: DropdownButton(
                                style: const TextStyle(height: 0.8),
                                underline: Container(),
                                value:
                                    dropdownvalue != "" ? dropdownvalue : null,
                                icon: const Icon(Icons.keyboard_arrow_down),
                                items: schoolRepo.stateModelList.state!
                                    .map((items) {
                                  return DropdownMenuItem<String>(
                                    onTap: () {
                                      selectStateID = items.stateId;
                                    },
                                    value: items.name,
                                    child: Container(
                                      margin: const EdgeInsets.only(
                                          left: 5, right: 4),
                                      child: Text(
                                        items.name!,
                                        style: const TextStyle(
                                            fontSize: 16,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                  );
                                }).toList(),
                                isExpanded: true,
                                // After selecting the desired option,it will
                                // change button value to selected value
                                onChanged: (String? newValue) {
                                  setState(() {
                                    dropdownvalue = newValue!;
                                  });
                                },
                              ),
                            );
                          },
                        ),
                      ),

                      const SizedBox(
                        height: 10.0,
                      ),
                      //State
                      const Text(
                        "City",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      TextField(
                        controller: city,
                        style: const TextStyle(height: 0.8),
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1.0),
                              borderRadius: BorderRadius.circular(7)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color(0xff634DD1), width: 1.0),
                            borderRadius: BorderRadius.circular(7),
                          ),
                        ),
                      ),

                      const SizedBox(
                        height: 10.0,
                      ),
                      //Website
                      const Text(
                        "Pincode",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      TextField(
                        controller: pinCodeField,
                        keyboardType: TextInputType.number,
                        maxLength: 6,
                        style: const TextStyle(height: 0.8),
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(10),
                          counterText: "",
                          fillColor: Colors.white,
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 1.0),
                              borderRadius: BorderRadius.circular(7)),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color(0xff634DD1), width: 1.0),
                            borderRadius: BorderRadius.circular(7),
                          ),
                        ),
                      ),

                      //Medium Of School
                      const SizedBox(
                        height: 10.0,
                      ),
                      const Text(
                        "Medium of Instructions",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 60,
                        child: DropDownMultiSelect(
                          onChanged: (List<String> x) {
                            mediumData = "";
                            x.forEach((element) {
                              schoolRepo.mediumModel.schoolList!.forEach((v) {
                                if (v.instruction == element) {
                                  if (mediumData.isEmpty) {
                                    mediumData += v.instructionId!.toString();
                                  } else {
                                    mediumData +=
                                        "," + v.instructionId!.toString();
                                  }
                                }
                              });
                            });
                            setState(() {
                              selected = x;
                            });
                          },
                          options: schoolRepo.mediumModel.medium,
                          selectedValues: selected,
                          whenEmpty: mediumDataNames,
                        ),
                      ),

                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: ValueListenableBuilder(
                            valueListenable: addSchoolValueNotifier,
                            builder: (context, v, c) {
                              return addSchoolValueNotifier.value == true
                                  ? Center(
                                      child: Container(
                                        height: 50,
                                        width: w * 0.3,
                                        decoration: BoxDecoration(
                                            color: Colors.deepPurple,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: const Center(
                                          child: CircularProgressIndicator(
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    )
                                  : GestureDetector(
                                      onTap: () {
                                        checkTextFieldEmptyOrNot();
                                      },
                                      child: Center(
                                        child: Container(
                                          height: 50,
                                          width: w * 0.3,
                                          decoration: BoxDecoration(
                                              color: Colors.deepPurple,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: const Center(
                                            child: Text(
                                              "Edit School",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16.0),
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                            }),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
