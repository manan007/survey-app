import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:survey/helper/global_variable.dart';

import '../helper/const.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  TextEditingController? fullName = TextEditingController();
  TextEditingController? email = TextEditingController();
  TextEditingController? phoneNumber = TextEditingController();
  TextEditingController? aboutMe = TextEditingController();
  ValueNotifier<bool> profileApiCall = ValueNotifier(false);

  String _image = "";
  final ImagePicker _picker = ImagePicker();
  PickedFile? file;

  Future selectImageFromGallery(quality) async {
    final XFile? image = await _picker.pickImage(
        source: ImageSource.gallery, imageQuality: quality);

    if (image == null) {
      return "";
    } else {
      print(image.path);
      return image.path;
    }
  }

  Future selectImageFromCamera(quality) async {
    final XFile? image = await _picker.pickImage(
        source: ImageSource.camera, imageQuality: quality);

    if (image == null) {
      return "";
    } else {
      print(image.path);
      return image.path;
    }
  }

  void checkFFieldEmptyOrNot() async {
    if (_image == "") {
      Fluttertoast.showToast(msg: "Please Select Profile Photo");
    } else if (fullName!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter Your Full Name");
    } else if (email!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter Your Email");
    } else if (phoneNumber!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter Phone Number");
    } else if (selectStateID == 0) {
      Fluttertoast.showToast(msg: "Please Select State");
    } else if (aboutMe!.text == "") {
      Fluttertoast.showToast(msg: "Please Enter About me");
    } else {
      profileApiCall.value = true;

      // String? profileImage,String? aboutMe
      // SharedPreferences preferences = await SharedPreferences.getInstance();
      // preferences.setString("fullName", fullName!.text);
      // preferences.setString("email", email!.text);
      // preferences.setString("phoneNumber", phoneNumber!.text);
      // preferences.setString("aboutMe", aboutMe!.text);
      // preferences.setString("selectState", dropdownvalue);
      // preferences.setString("image", _image);

      //API Call

      bool result = await profileRepo.updateProfile(
        1,
        selectStateID,
        fullName!.text,
        email!.text,
        phoneNumber!.text,
        _image == "" ? profileRepo.getProfileModel.userlist![0].photo : _image,
        aboutMe!.text,
      );

      if (result) {
        profileApiCall.value = false;
        // Fluttertoast.showToast(msg: "Profile Add SuccessFully");
        fullName!.clear();
        email!.clear();
        phoneNumber!.clear();
        aboutMe!.clear();
        _image = "";
        selectStateID = 0;
        selectCountryId = 0;
        setState(() {});
        Fluttertoast.showToast(msg: "User Update successfully");

        await profileRepo.getProfile();

        Fluttertoast.cancel();
        Navigator.pop(context);
      } else {
        Fluttertoast.showToast(msg: "User Update successfully");
        await profileRepo.getProfile();
        Navigator.pop(context);
        profileApiCall.value = false;
      }
    }
  }

  bool isApiCalling = false;

  void getData() async {
    setState(() {
      isApiCalling = true;
    });
    await profileRepo.getAllCountry();
    await schoolRepo.getAllState();
    await profileRepo.getProfile();
    setData();
    getSharedPrefs();
    setState(() {
      isApiCalling = false;
    });
  }

  setData() {
    fullName!.text = profileRepo.getProfileModel.userlist![0].fullName!;
    email!.text = profileRepo.getProfileModel.userlist![0].email!;
    phoneNumber!.text = profileRepo.getProfileModel.userlist![0].mobileNo!;
    aboutMe!.text = profileRepo.getProfileModel.userlist![0].aboutMe!;
    selectStateID = profileRepo.getProfileModel.userlist![0].stateId!;
  }

  String dropdownvalue = '';
  int? selectStateID;
  String countryName = 'india';
  int? selectCountryId;

  @override
  void dispose() {
    fullName!.dispose();
    email!.dispose();
    phoneNumber!.dispose();
    aboutMe!.dispose();
    super.dispose();
  }

  @override
  void initState() {
    getData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        leading: Builder(
            builder: (context) => IconButton(
                  onPressed: () {
                    setState(() {
                      Navigator.pop(context);
                    });
                  },
                  icon: Icon(Icons.arrow_back),
                  color: Colors.black,
                )),
        backgroundColor: Colors.white,
        title: Container(
            height: 70,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/Band 04 copy.png'))),
            child: Center(child: Text("Edit Profile"))),
      ),
      body: isApiCalling
          ? const Center(child: CircularProgressIndicator())
          : SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.only(left: 15, right: 15, top: 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Center(
                        child: Container(
                          height: 150,
                          width: 150,
                          decoration: BoxDecoration(
                              color: Colors.grey[200],
                              borderRadius: BorderRadius.circular(10)),
                          child: Column(
                            children: [
                              _image != ""
                                  ? Image.file(
                                      File(_image),
                                      height: 150,
                                      width: 150,
                                      fit: BoxFit.cover,
                                    )
                                  : Image.network(
                                      "${profileRepo.getProfileModel.userlist![0].photo}",
                                      height: 150,
                                      width: 150,
                                      fit: BoxFit.cover,
                                    ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              double h = MediaQuery.of(context).size.height;
                              double w = MediaQuery.of(context).size.width;
                              return Dialog(
                                backgroundColor: Color(0xc7f9f9f9),
                                child: Container(
                                  height: h * 0.12,
                                  width: w * 0.35,
                                  decoration: BoxDecoration(
                                    color: const Color(0xc7f9f9f9),
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Expanded(
                                          child: InkWell(
                                            onTap: () async {
                                              _image =
                                                  await selectImageFromCamera(
                                                      30);
                                              setState(() {});
                                              Navigator.pop(context);
                                            },
                                            splashColor:
                                                Colors.grey.withOpacity(0.7),
                                            child: Container(
                                              color: Colors.transparent,
                                              child: const Center(
                                                child: Text(
                                                  "Camera",
                                                  style: TextStyle(
                                                      color: Color(0xff0089f1),
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontFamily: 'Raleway',
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontSize: 17.0),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        const Divider(
                                          height: 0.0,
                                          color: Colors.grey,
                                        ),
                                        Expanded(
                                          child: InkWell(
                                            onTap: () async {
                                              _image =
                                                  await selectImageFromGallery(
                                                      30);
                                              setState(() {});
                                              Navigator.pop(context);
                                            },
                                            splashColor:
                                                Colors.grey.withOpacity(0.7),
                                            child: Container(
                                              color: Colors.transparent,
                                              child: const Center(
                                                child: Text(
                                                  "Gallery",
                                                  style: TextStyle(
                                                      color: Color(0xff0089f1),
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontFamily: 'Raleway',
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontSize: 17.0),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            });
                      },
                      child: Center(
                        child: Container(
                          height: 40,
                          width: w * 0.3,
                          decoration: BoxDecoration(
                            border: Border.all(color: backGroundColor),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Center(
                            child: Text("Upload Image"),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: h * 0.01,
                    ),
                    GestureDetector(
                      onTap: () {
                        print(userId.value);
                      },
                      child: const Text(
                        "Full Name",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    TextField(
                      controller: fullName,
                      // style: const TextStyle(height: 1.),
                      decoration: InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Your full Name",
                        enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 1.0),
                            borderRadius: BorderRadius.circular(7)),
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color(0xff634DD1), width: 1.0),
                          borderRadius: BorderRadius.circular(7),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    const Text(
                      "Email",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                    TextField(
                      keyboardType: TextInputType.emailAddress,
                      controller: email,
                      // style: const TextStyle(height: 0.5),
                      decoration: InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Your email",
                        enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 1.0),
                            borderRadius: BorderRadius.circular(7)),
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color(0xff634DD1), width: 1.0),
                          borderRadius: BorderRadius.circular(7),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    const Text(
                      "Phone Number",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                    TextField(
                      keyboardType: TextInputType.phone,
                      controller: phoneNumber,
                      // style: const TextStyle(height: 0.3),
                      decoration: InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Your Phone Number",
                        enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 1.0),
                            borderRadius: BorderRadius.circular(7)),
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color(0xff634DD1), width: 1.0),
                          borderRadius: BorderRadius.circular(7),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    const Text(
                      "State",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 55,
                      child: FormField(
                        builder: (FormFieldState<String> state) {
                          return InputDecorator(
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(6.0),
                              fillColor: Colors.white,
                              filled: true,
                              // hintText: "Your Phone Number",
                              enabledBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Colors.grey, width: 1.0),
                                  borderRadius: BorderRadius.circular(7)),
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Color(0xff634DD1), width: 1.0),
                                borderRadius: BorderRadius.circular(7),
                              ),
                            ),
                            child: DropdownButton(
                              style: const TextStyle(height: 0.8),
                              underline: Container(),
                              value: dropdownvalue != ""
                                  ? dropdownvalue
                                  : profileRepo
                                      .getProfileModel.userlist![0].state!,
                              icon: const Icon(Icons.keyboard_arrow_down),
                              items:
                                  schoolRepo.stateModelList.state!.map((items) {
                                return DropdownMenuItem<String>(
                                  onTap: () {
                                    selectStateID = items.stateId;
                                  },
                                  value: items.name,
                                  child: Container(
                                    margin: const EdgeInsets.only(
                                        left: 5, right: 4),
                                    child: Text(
                                      items.name!,
                                      style: const TextStyle(
                                          fontSize: 16,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                );
                              }).toList(),
                              isExpanded: true,
                              // After selecting the desired option,it will
                              // change button value to selected value
                              onChanged: (String? newValue) {
                                setState(() {
                                  dropdownvalue = newValue!;
                                });
                              },
                            ),
                          );
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    const Text(
                      "About Me",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                    TextField(
                      maxLines: 3,
                      controller: aboutMe,
                      // style: const TextStyle(height: 0.8),
                      decoration: InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Tell Something about YourSelf",
                        enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 1.0),
                            borderRadius: BorderRadius.circular(7)),
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color(0xff634DD1), width: 1.0),
                          borderRadius: BorderRadius.circular(7),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: ValueListenableBuilder(
                          valueListenable: profileApiCall,
                          builder: (context, v, c) {
                            return profileApiCall.value == true
                                ? Center(
                                    child: Container(
                                      height: 50,
                                      width: w * 0.3,
                                      decoration: BoxDecoration(
                                          color: Colors.deepPurpleAccent,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: const Center(
                                          child: CircularProgressIndicator(
                                        color: Colors.white,
                                      )),
                                    ),
                                  )
                                : GestureDetector(
                                    onTap: () async {
                                      checkFFieldEmptyOrNot();
                                    },
                                    child: Center(
                                      child: Container(
                                        height: 50,
                                        width: w * 0.3,
                                        decoration: BoxDecoration(
                                            color: backGroundColor,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: const Center(
                                          child: Text(
                                            "Update Profile",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w600,
                                                fontSize: 15),
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                          }),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  getSharedPrefs() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    _image = preferences.getString("image")!;
  }
}
