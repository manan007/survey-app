import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:survey/View/home_page.dart';
import 'package:survey/View/new.dart';
import 'package:survey/View/school_list.dart';
import 'package:survey/helper/const.dart';
import 'package:survey/helper/global_variable.dart';

import 'edit_profile.dart';

class DashBord extends StatefulWidget {
  const DashBord({Key? key}) : super(key: key);

  @override
  _DashBordState createState() => _DashBordState();
}

class _DashBordState extends State<DashBord> {
  String _image = "";
  String _name = "";
  bool isApiCalling = false;

  @override
  void initState() {
    super.initState();

    getSharedPrefs();
  }

  Future<String?> getSharedPrefs() async {
    setState(() {});
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      _image = preferences.getString("image")!;
      _name = preferences.getString('fullName')!;
    });
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        leading: Builder(
            builder: (context) => IconButton(
                  onPressed: () {
                    setState(() {
                      Scaffold.of(context).openDrawer();
                    });
                  },
                  icon: Icon(Icons.menu),
                  color: Colors.black,
                )),
        backgroundColor: Colors.white,
        title: Container(
            height: 70,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/Band 04 copy.png'))),
            child: Center(child: Text("Welcome to School Survey"))),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddSchoolUI()));
              },
              child: Container(
                margin: const EdgeInsets.only(left: 20, right: 20),
                height: h * 0.3,
                width: w,
                color: Colors.transparent,
                child: Column(
                  children: [
                    SchoolSurveyWidgets(
                      imageName: 'assets/images/add_school.jpg',
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    const Text(
                      "Add School",
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Color(0xff08C4E2)),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                if (kDebugMode) {
                  print(userId.value);
                }
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const SchoolList(),
                  ),
                );
              },
              child: Container(
                margin: const EdgeInsets.only(left: 20, right: 20),
                height: h * 0.3,
                width: w,
                color: Colors.transparent,
                child: Column(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: SchoolSurveyWidgets(
                        imageName: 'assets/images/Page 6_2_stand.png',
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    const Text(
                      "School List",
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Color(0xff08C4E2)),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      drawer: Drawer(
        child: Column(
          children: [
            DrawerHeader(
              child: CircleAvatar(
                  backgroundColor: Colors.blue,
                  radius: 100,
                  child: ClipOval(
                    child: ValueListenableBuilder(
                        valueListenable: profileImageForUser,
                        builder: (context, v, c) {
                          return Image.network(
                            profileImageForUser.value,
                            height: 150.0,
                            width: 140.0,
                            fit: BoxFit.cover,
                          );
                        }),
                  )),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                    width: 150,
                    child: Center(
                      child: ValueListenableBuilder(
                          valueListenable: profileNameForUser,
                          builder: (context, v, c) {
                            return Text(
                              profileNameForUser.value,
                              maxLines: 2,
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold),
                            );
                          }),
                    )),
              ],
            ),
            const SizedBox(
              height: 30.0,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      if (kDebugMode) {
                        print(userId.value);
                      }

                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const EditProfile()));
                    },
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/user.png',
                          width: 30,
                          height: 30,
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 15),
                          child: const Text(
                            "Profile",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 15.0,
                  ),
                  const Divider(
                    height: 10.0,
                    color: Colors.grey,
                  ),
                  const SizedBox(
                    height: 15.0,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              content: const Text(
                                  "Are you sure you want to Logout?"),
                              actions: [
                                ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text("NO"),
                                ),
                                ElevatedButton(
                                  onPressed: () async {
                                    Navigator.pushAndRemoveUntil<void>(
                                      context,
                                      MaterialPageRoute<void>(
                                        builder: (BuildContext context) =>
                                            const HomePage(),
                                      ),
                                      (Route<dynamic> route) => false,
                                    );
                                    SharedPreferences preferences =
                                        await SharedPreferences.getInstance();
                                    preferences.clear();
                                  },
                                  child: const Text("YES"),
                                ),
                              ],
                            );
                          });
                    },
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/logout.png',
                          color: Colors.blue[500],
                          width: 30,
                          height: 30,
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 15),
                          child: const Text(
                            "Logout",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SchoolSurveyWidgets extends StatelessWidget {
  String? imageName;

  SchoolSurveyWidgets({Key? key, required this.imageName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return SizedBox(
      height: h * 0.22,
      width: w * 0.7,
      child: Image.asset(
        imageName!,
        fit: BoxFit.fill,
        // width: MediaQuery.of(context).size.width / 2,
      ),
    );
  }
}
