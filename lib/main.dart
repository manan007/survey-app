import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:survey/View/dashbord.dart';
import 'package:survey/View/home_page.dart';
import 'package:survey/View/school_survey.dart';
import 'package:survey/View/splash_screen.dart';
import 'package:survey/helper/PageTransitionType.dart';
import 'package:survey/helper/global_variable.dart';

import 'helper/const.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  late SharedPreferences preferences;

  bool isLogin = false;
  bool isCheckIng = false;

  Future loginCheck() async {
    setState(() {
      isCheckIng = true;
    });
    Position position = await _getGeoLocationPosition();
    location = 'Lat: ${position.latitude} , Long: ${position.longitude}';
    GetAddressFromLatLong(position);
    preferences = await SharedPreferences.getInstance();
    isLogin = preferences.getBool("isLogin") ?? false;
    userId.value = preferences.getInt("userid") ?? 0;
    setState(() {
      isCheckIng = false;
    });
  }

  Future<Position> _getGeoLocationPosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      await Geolocator.openLocationSettings();
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  Future<void> GetAddressFromLatLong(Position position) async {
    List<Placemark> placemarks =
        await placemarkFromCoordinates(position.latitude, position.longitude);

    Placemark place = placemarks[0];
    Address =
        '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    address!.text =
        '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    setState(() {});
  }

  @override
  void initState() {
    loginCheck();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Survey',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: splash_screen(),
        onGenerateRoute: (RouteSettings settings) {
          List<dynamic> Details;

          switch (settings.name) {
            case '/DashBord':
              return PageTransition(
                  child: const DashBord(),
                  type: PageTransitionType.leftToRight,
                  settings: settings);

            case '/HomePage':
              return PageTransition(
                  child: const HomePage(),
                  type: PageTransitionType.leftToRight,
                  settings: settings);
            case '/SchoolSurvey':
              return PageTransition(
                  child: SchoolSurvey(),
                  type: PageTransitionType.leftToRight,
                  settings: settings);
          }
        });
  }
}
