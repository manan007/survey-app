import 'dart:convert' as convert;

import 'package:flutter/foundation.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:survey/helper/global_variable.dart';
import 'package:survey/model/medium_model.dart';
import 'package:survey/model/school_model.dart';
import 'package:survey/model/state_model.dart';

class SchoolRepo {
  //This Object  Use For SchoolList Model  Object
  GetSchoolModel getSchoolModel = GetSchoolModel();

  //This Object  Use For  Medium Model  Object
  MediumModel mediumModel = MediumModel();
  ValueNotifier<bool> noSchoolFound = ValueNotifier(false);

  //This Object  Use For  State  Model  Object
  StateModel stateModelList = StateModel();
  List<States> stateModel = [];

  //This Function use for getUserid To  School List
  Future<bool> getSchoolList() async {
    print("---- ${userId.value}");
    try {
      http.Response response = await http.get(Uri.parse(
          "$baseUrl/School/GetAllSchoolsbyUserid?userid=${userId.value}"));
      print("******************* ${response.body}");
      var data = convert.jsonDecode(response.body);
      if (response.statusCode == 200 && data['success'] == true) {
        getSchoolModel = GetSchoolModel.fromJson(data);
        // print("************************** School list" +
        //     getSchoolModel.schoolList!.toString());
        return true;
      } else if (response.statusCode == 200 && data['success'] == false) {
        noSchoolFound.value = true;
        return false;
      } else if (response.statusCode == 500) {
        noSchoolFound.value = true;
        return false;
      }
      return false;
    } catch (e) {
      if (kDebugMode) {
        print("Get School List Api Call  Error $e");
      }
      return false;
    }
  }

  //This Function Use For Add School
  Future<bool> addSchool(
    String? schoolName,
    String? address,
    String? latitude,
    String? phoneNumber,
    String? email,
    String? website,
    int? stateId,
    String? city,
    int? pinCode,
    String? medium,
    int? userId,
  ) async {
    try {
      http.Response response =
          await http.post(Uri.parse("$baseUrl/School/AddSchool"),
              headers: {
                "content-type": "application/json",
                "Accept": "application/json",
              },
              body: convert.jsonEncode({
                "Name": "$schoolName",
                "Address": "$address",
                "LatLong": "$latitude",
                "PhoneNumber": "$phoneNumber",
                "Email": "$email",
                "Website": "$website",
                "StateId": stateId,
                "City": "$city",
                "PinCode": pinCode,
                "CreatedBy": userId,
                "MediumId": "$medium"
              }));
      print(response.body);
      var data = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        Fluttertoast.showToast(msg: data['message']);
        return true;
      } else {
        Fluttertoast.showToast(msg: data['message']);
        return false;
      }
    } catch (e) {
      print("Add School API Call Error $e");
      return false;
    }
  }

  //This Function use  for Get  All  State
  Future<bool> getAllState() async {
    try {
      http.Response response =
          await http.get(Uri.parse("$baseUrl/UserProfiles/GetAllStates"));
      var data = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        stateModelList = StateModel.fromJson(data);
        stateModel = stateModelList.state!;
        print("schoolModelList " + stateModelList.state!.toList().toString());
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print("Get All State API Call Error $e");
      return false;
    }
  }

  //This Function Use For Get All School Medium
  Future<bool> getAllMedium() async {
    try {
      http.Response response =
          await http.get(Uri.parse("$baseUrl/School/GetMediums"));
      var data = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        mediumModel = MediumModel.fromJson(data);
        print(mediumModel.schoolList!.length);
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print("Get  All Medium Api  Call Error $e");
      return false;
    }
  }

  //This is function is used for edit school details
  Future<bool> editSchool(
    int? schoolID,
    String? schoolName,
    String? address,
    String? latitude,
    String? phoneNumber,
    String? email,
    String? website,
    int? stateId,
    String? city,
    int? pinCode,
    String? medium,
    int? userId,
  ) async {
    try {
      http.Response response =
          await http.post(Uri.parse("$baseUrl/School/UpdateSchool"),
              headers: {
                "content-type": "application/json",
                "Accept": "application/json",
              },
              body: convert.jsonEncode({
                "SchoolId": schoolID,
                "Name": "$schoolName",
                "Address": "$address",
                "LatLong": "$latitude",
                "PhoneNumber": "$phoneNumber",
                "Email": "$email",
                "Website": "$website",
                "StateId": stateId,
                "City": "$city",
                "PinCode": pinCode,
                "CreatedBy": userId,
                "MediumId": "$medium"
              }));
      print("******************${response.body}");
      var data = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        Fluttertoast.showToast(msg: data['message']);
        return true;
      } else {
        Fluttertoast.showToast(msg: data['message']);
        return false;
      }
    } catch (e) {
      print("Add School API Call Error $e");
      return false;
    }
  }

  //This Function is used for delete school
  Future<bool> deleteSchool(
    int? schoolID,
  ) async {
    print("------------ $schoolID");
    try {
      http.Response response = await http.post(
        Uri.parse("$baseUrl/School/DeleteSchool?SchoolId=$schoolID"),
      );
      print("******************${response.body}");
      var data = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        Fluttertoast.showToast(msg: data['message']);
        return true;
      } else {
        Fluttertoast.showToast(msg: data['message']);
        return false;
      }
    } catch (e) {
      print("Add School API Call Error $e");
      return false;
    }
  }

  Future<bool> addSchoolSurvey(int? schoolID) async {
    try {
      http.Response response = await http.post(
          Uri.parse(
            "$baseUrl/Survey/AddSchoolSurvey",
          ),
          headers: {
            "content-type": "application/json",
            "Accept": "application/json",
          },
          body: convert.jsonEncode({"SchoolId": schoolID}));
      print("************Add School Survey ******${response.body}");
      var data = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        Fluttertoast.showToast(msg: data['message']);
        return true;
      } else {
        Fluttertoast.showToast(msg: data['message']);
        return false;
      }
    } catch (e) {
      print("Add School API Call Error $e");
      return false;
    }
  }
}
