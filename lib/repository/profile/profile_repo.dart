import 'dart:convert';
import 'dart:convert' as convert;
import 'dart:io';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:survey/helper/global_variable.dart';
import 'package:survey/model/country_model.dart';
import 'package:survey/model/get_profile_model.dart';

class ProfileRepo {
  //This Object is Country Model
  CountryModel countryModel = CountryModel();

  //This Object is Use for get Profile Data
  GetProfileModel getProfileModel = GetProfileModel();

  //This Function use For Get all Country
  Future<bool> getAllCountry() async {
    try {
      http.Response response =
          await http.get(Uri.parse("$baseUrl/UserProfiles/GetAllCountries"));
      var data = jsonDecode(response.body);
      if (response.statusCode == 200) {
        countryModel = CountryModel.fromJson(data);
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print("Get AllCountry API Error $e");
      return false;
    }
  }

  // This Function  Use Add Profile
  Future<bool> addProfile(
      int? countryId,
      int? stateId,
      String? name,
      String? email,
      String? mobileNumber,
      String? profileImage,
      String? aboutMe) async {
    try {
      print(profileImage);

      final imageUploadRequest = http.MultipartRequest(
          'POST', Uri.parse("$baseUrl/UserProfiles/UpdateUser"))
        ..headers;
      final file =
          await http.MultipartFile.fromPath('SpeakerPicture', profileImage!);

      imageUploadRequest.files.add(file);

      imageUploadRequest.fields['UserId'] = userId.value.toString();
      imageUploadRequest.fields['AboutMe'] = aboutMe!;
      imageUploadRequest.fields['CountryId'] = countryId.toString();
      imageUploadRequest.fields['CreatedBy'] = userId.value.toString();
      imageUploadRequest.fields['Full_Name'] = name!;
      imageUploadRequest.fields['Email'] = email!;
      imageUploadRequest.fields['MobileNo'] = mobileNumber!;
      imageUploadRequest.fields['RoleId'] = 2.toString();
      imageUploadRequest.fields['StateId'] = stateId.toString();

      try {
        final streamedResponse = await imageUploadRequest.send();
        http.Response response =
            await http.Response.fromStream(streamedResponse);
        var data = jsonDecode(response.body);

        print(response.body);
        getProfile();

        return true;
      } catch (e) {
        return false;
      }
    } catch (e) {
      print("Add Profile API Call Error $e");
      return false;
    }
  }

  //Update Profile

  Future<bool> updateProfile(
      int? countryId,
      int? stateId,
      String? name,
      String? email,
      String? mobileNumber,
      String? profileImage,
      String? aboutMe) async {
    try {
      final imageUploadRequest = http.MultipartRequest(
          'POST', Uri.parse("$baseUrl/UserProfiles/UpdateUser"))
        ..headers;
      final file =
          await http.MultipartFile.fromPath('SpeakerPicture', profileImage!);

      imageUploadRequest.files.add(file);

      imageUploadRequest.fields['UserId'] = userId.value.toString();
      imageUploadRequest.fields['AboutMe'] = aboutMe!;
      imageUploadRequest.fields['CountryId'] = countryId.toString();
      imageUploadRequest.fields['CreatedBy'] = userId.value.toString();
      imageUploadRequest.fields['Full_Name'] = name!;
      imageUploadRequest.fields['Email'] = email!;
      imageUploadRequest.fields['MobileNo'] = mobileNumber!;
      imageUploadRequest.fields['RoleId'] = 2.toString();
      imageUploadRequest.fields['StateId'] = stateId.toString();

      try {
        final streamedResponse = await imageUploadRequest.send();
        http.Response response =
            await http.Response.fromStream(streamedResponse);
        var data = jsonDecode(response.body);

        //print("Update API ${response.body}");

        profileImageForUser.value = data['userlist'][0]['Photo'];
        profileNameForUser.value = data['userlist'][0]['Full_Name'];

        print(data['userlist'][0]['Photo']);
        print(data['userlist'][0]['Full_Name']);

        getProfile();
        return true;
      } catch (e) {
        return false;
      }
    } catch (e) {
      print("Add Profile API Call Error $e");
      return false;
    }
  }

  //Get  Profile By  User id
  Future<bool> getProfile() async {
    try {
      http.Response response = await http.get(Uri.parse(
          "$baseUrl/UserProfiles/GetUserbyId?userid=${userId.value}"));

      var data = jsonDecode(response.body);

      if (response.statusCode == 200 && data['success'] == true) {
        getProfileModel = GetProfileModel.fromJson(data);

        print(getProfileModel.userlist![0].fullName);

        profileImageForUser.value = getProfileModel.userlist![0].photo!;
        profileNameForUser.value = getProfileModel.userlist![0].fullName!;

        return true;
      } else {
        return false;
      }
    } catch (e) {
      print("Get Profile API Call Error $e");
      return false;
    }
  }
}
