import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:survey/helper/const.dart';

import '../../helper/global_variable.dart';

class ServerAuth {
//This Function  use for Severauth

  //This Function Use For Email To Login
  Future<bool> emailLogin(String? email) async {
    try {
      http.Response response = await http.post(
        Uri.parse("$baseUrl/UserProfiles/GetUserProfilebyEmail/$email"),
      );
      var data = jsonDecode(response.body);
      if (response.statusCode == 200) {
        Fluttertoast.showToast(msg: "${data['message']}");
        storeOtpNumber.value = data['Otp'];
        print(storeOtpNumber.value);
        return true;
      } else {
        return false;
      }
    } catch (e) {
      if (kDebugMode) {
        print("EmailLogin API Call Error $e");
      }
      return false;
    }
  }

  //This Function  Use  For Otp Verification
  Future<bool> otpVerification(int? otpNumber, String email) async {
    try {
      http.Response response = await http.post(
        Uri.parse(
            "$baseUrl/UserProfiles/VerifyUserProfilesfromOTP?OTP=$otpNumber&email=$email"),
      );
      var data = jsonDecode(response.body);
      if (response.statusCode == 200) {
        print(response.body);
        Fluttertoast.showToast(msg: "${data['message']}");

        SharedPreferences preferences = await SharedPreferences.getInstance();

        preferences.setBool("isLogin", true);

        preferences.setInt("userid", data['userid']);

        userId.value = data['userid'];
        isLoggedIn.value = true;
        if (data['is_new']) {
          preferences.setBool("isNew", true);
        } else {
          preferences.setBool("isNew", false);
        }
        return true;
      } else {
        return false;
      }
    } catch (e) {
      if (kDebugMode) {
        print("Otp Verification API Call error $e");
      }
      return false;
    }
  }
}
