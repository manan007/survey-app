import 'package:flutter/cupertino.dart';
import 'package:survey/repository/profile/profile_repo.dart';
import 'package:survey/repository/schoolrepo/school_repo.dart';
import 'package:survey/repository/serverauth/server_auth.dart';

//Server Auth Global Variable
ServerAuth serverAuth = ServerAuth();
//School List  Global  Variable
SchoolRepo schoolRepo =  SchoolRepo();
//This Profile Repo
ProfileRepo profileRepo =  ProfileRepo();

String  baseUrl  = "https://www.theprojects.in/survey";

//Store Otp  Number
ValueNotifier<int> storeOtpNumber = ValueNotifier(0);
//This ValueNotifier Use for User Id Store
ValueNotifier<int> userId = ValueNotifier(0);

//Store Image For Data
ValueNotifier<String>  profileImageForUser =  ValueNotifier("");
ValueNotifier<String>  profileNameForUser =  ValueNotifier("");
